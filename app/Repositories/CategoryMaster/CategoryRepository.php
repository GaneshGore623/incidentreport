<?php

namespace App\Repositories\CategoryMaster;

use App\Repositories\RepositoryAbstract;
use App\Models\CategoryMaster;
use Illuminate\Support\Facades\DB;

class CategoryRepository extends RepositoryAbstract implements CategoryInterface
{
	protected $model;

	public function __construct(CategoryMaster $model)
	{
		$this->model = $model;
    }

    public function saveCategory(array $attributes)
    {
        $insert_category = $this->model->insert($attributes);
        return $insert_category;
    }


}