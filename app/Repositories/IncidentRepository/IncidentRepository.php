<?php

namespace App\Repositories\IncidentRepository;

use App\Repositories\RepositoryAbstract;
use App\Models\IncidentReport;
use Illuminate\Support\Facades\DB;

class IncidentRepository extends RepositoryAbstract implements IncidentInterface
{

	protected $model;

	public function __construct(IncidentReport $model)
	{
		$this->model = $model;
    }

    public function saveIncidentReport(array $attributes)
    {
        $insert_incident = $this->model->insert($attributes);
        return $insert_incident;
    }

    public function getIncidentReportList()
    {
        $insert_incident = $this->model->get();
        return $insert_incident;
    }
}