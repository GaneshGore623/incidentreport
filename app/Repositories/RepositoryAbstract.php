<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;
use Illuminate\Support\Facades\DB;

abstract class RepositoryAbstract implements RepositoryInterface
{

	protected $model;

	//Common Repositories
	public function getAll()
	{
		return $this->model->all();
	}

	public function getById($id)
	{
		return $this->model->findOrFail($id);
	}

	public function create(array $attributes)
	{
		return $this->model->create($attributes);
	}

	public function update($id, array $attributes)
	{
		$update = $this->model->findOrFail($id);
		$update->update($attributes);

		return $update;
	}

	public function delete($id)
	{
		$this->model->getById($id)->delete();
		return true;
	}

	public function checkMobileNoExists ( $mobile_no ) {
		return $this->model::where('mobile_no', $mobile_no)->first();
	}

	public function updateWhere ( $where, $attributes ) {
		return $this->model::where($where)
          ->update($attributes);
	}

	public function selectWhere ( $where, $attributes ) {
		return $this->model::select($attributes)->where($where)->first();
	}

	public function selectWhereAll ( $where, $attributes ) {
		return $this->model::select($attributes)->where($where)->get();
	}

	public function getAdmin ( $admin_id ) {
		return $this->model::where('id', $admin_id)->get();
	}

	public function insertOrUpdate(array $attributes, array $where)
	{
		return $this->model->updateOrInsert($where, $attributes);
	}

	//Common Repositories


}
