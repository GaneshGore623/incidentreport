<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\IncidentRepository\IncidentInterface;
use App\Repositories\IncidentRepository\IncidentRepository;
use App\Repositories\CategoryMaster\CategoryInterface;
use App\Repositories\CategoryMaster\CategoryRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(IncidentInterface::class, IncidentRepository::class);
        $this->app->singleton(CategoryInterface::class, CategoryRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
