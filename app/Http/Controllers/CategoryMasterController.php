<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Repositories\CategoryMaster\CategoryInterface;

class CategoryMasterController extends MainController
{
	private $incident;
	public function __construct(Request $request, CategoryInterface $category_master)

	{
        parent::__construct($request);
       	$this->category_master = $category_master;
    }

    public function saveCategory(Request $request){
    	$category_name = $request->post('category_name');

    	if ($category_name !='')
        {
            $args = array(
                'category_name' => $category_name,
                'created_date' => date('Y-m-d H:s:i')
            );

            $result = $this->category_master->saveCategory($args);

            if ($result == NULL || empty($result) || $result == '') {
                   return Response::json(['data' => 'Category is not added.'], 200);
            } else {
                   return Response::json(['data' => $args], 200);
            }

        } else {
            return Response::json(['data' => 'Category Cannot be empty!'], 200);;
	    }
    }
}
