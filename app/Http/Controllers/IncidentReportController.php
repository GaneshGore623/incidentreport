<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Repositories\IncidentRepository\IncidentInterface;
use Validator;

class IncidentReportController extends MainController
{
	private $incident;
	public function __construct(Request $request, IncidentInterface $incident)

	{
        parent::__construct($request);
       	$this->incident = $incident;
    }

    public function saveIncidentReport(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'latitude' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'longitude' => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            'category' => 'required',
            'incident_date' => 'required|date',
            'created_date' => 'required|date',
            'updated_date' => 'required|date',
        ]);

        if ($validator->fails()) {
            $message = $validator->errors();
            return Response::json(['message' => $message], 422);
        }

        if( $request->post('name') !='' ){
    		$name = json_encode($request->post('name'));
    	}else{
    		$name = $request->post('name');
    	}
    	
    	if( $request->post('created_date') !='' && $request->post('updated_date') !="" ){
    		$created_date = date('Y-m-d H:s:i',strtotime($request->post('created_date')));
    		$updated_date = date('Y-m-d H:s:i',strtotime($request->post('updated_date')));
    	}else{
    		$created_date = date('Y-m-d H:s:i');
    		$updated_date = date('Y-m-d H:s:i');
    	}
    	
        $args = array(
            'latitude' => $request->post('latitude'),
            'longitude' => $request->post('longitude'),
            'title' => $request->post('title'),
            'category' => $request->post('category'),
            'people' => $name,
            'comments' => $request->post('comments'),
            'incident_date' => date('Y-m-d H:s:i',strtotime($request->post('incident_date'))),
            'created_date' => $created_date,
            'updated_date' => $updated_date,
        );

        $result = $this->incident->saveIncidentReport($args);

        if ($result == NULL || empty($result) || $result == '') {
               return Response::json(['data' => 'Incident Report is not added.'], 200);
        } else {
               return Response::json(['data' => $args, 'message' => 'Incident Report added successfully!'], 200);
        }
    }    

    public function getIncidentReport(Request $request)
    {
        $result = $this->incident->getIncidentReportList();
        foreach ($result as $res_key => $res_value) {
        		$location['latitude'] = $res_value->latitude;
        		$location['longitude'] = $res_value->longitude;
        		$res_value->location = $location;
        		unset($res_value->latitude);
        		unset($res_value->longitude);
        		$res_value->people = json_decode($res_value->people);
        }

        if ($result == NULL || empty($result) || $result == '') {
               return Response::json(['data' => 'Incident Report data is not added.'], 200);
        } else {
               return Response::json(['data' => $result], 200);
        }
    }
}
