<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MainController extends Controller
{

    public $admin_user_id;
    public $user_id;

    public function __construct(Request $request)
    {
        //parent::__construct();
    }

    public function uploadProductImagesS3 ( $data ) {
    	try {
    		foreach ($data as $key => $value) {
                // print_r($value);exit;
                // dd(Storage::disk('s3')->directories('pdf/ack'));
    			return Storage::disk('s3')->put($value['s3_path'], file_get_contents($value['file_path']), 'public');
    		}
	    } catch ( \Exception $e ) {
            throw new \Exception("Error Processing Request - " . $e->getMessage(), 1); 
        }
    }

    public function checkOrCreateS3YearWiseFolderExists ( $folder_name ) {
        $s3_pdf_folder_path = Storage::disk('s3')->directories($folder_name);

        $current_year = date('Y');
        $s3_current_year_directory = $folder_name.'/'.$current_year;

        if( array_key_exists(0, $s3_pdf_folder_path) ) {
            $explode_s3_path = explode('/', $s3_pdf_folder_path[0]);    

            if( array_key_exists(1, $explode_s3_path) && ( $current_year == $explode_s3_path[1] ) ) {
                return true;
            } else {
                return Storage::disk('s3')->makeDirectory($s3_current_year_directory);
            }    
        } else {
            return Storage::disk('s3')->makeDirectory($s3_current_year_directory);
        }
    }

    public function checkFileExist($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
 
        if ($code == 200) {
            $status = true;
        } else {
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

}
