<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

        public function respond($data)
    {
        if ($this->httpStatus == 200 && $this->status) {
            return response()
                ->json([
                    'status' => $this->status,
                    'data' => $data
                ])
                ->setStatusCode($this->httpStatus);
        }
        return response()
            ->json([
                'status' => $this->status,
                'message' => $data
            ])
            ->setStatusCode($this->httpStatus);
    }

    public function respondWithSuccess($data)
    {
        $this->httpStatus = 200;
        $this->status = 200;
        return $this->respond($data);
    }

    public function respondWithFailure($message)
    {
        $this->httpStatus = 0;
        $this->status = 0;
        return $this->respond($message);
    }
    public function respondWithDataNotFoundMessage($message)
    {
        $this->status = 0;
        $this->httpStatus = 204;
        return $this->respond($message);
    }

    public function respondWithValidationError($message, $web = false)
    {
        $this->httpStatus = 422;
        $this->status = 0;
        if (gettype($message) == 'string' || gettype($message) == 'array') {
            return $this->respond($message);
        }
        return $this->respond($web ? $message : $message->first());
    }

    public function respondWithError($message)
    {
        $this->httpStatus = 500;
        $this->status = 0;
        return $this->respond($message);
    }

    public function respondUnauthorised($message)
    {
        $this->httpStatus = 401;
        $this->status = 0;
        return $this->respond($message);
    }

    public function authUser(Request $request)
    {
        try {
            if ($request->query('tokenize') && $request->query('mobile_no') && $request->query('user_type')) {

                $user_type = $request->query('user_type');
                $where = [
                    'token' => $request->query('tokenize'),
                    'mobile_no' => $request->query('mobile_no')
                ];

                $select = ['id', 'user_type', 'mobile_no', 'token'];

                switch ($user_type) {
                    case 'parent':
                        $result = $this->franchise->selectWhere($where, $select);

                        if ($result == NULL || empty($result) || $result == '') {
                            $data = $this->setUserAuthCookie($result, 'parent-dashboard', 'parent_id', $result->user_type);

                            return redirect($data['url'])
                                ->withCookie($data['cookie'])
                                ->withCookie($data['cookie_userid'])
                                ->withCookie($data['cookie_loginusertype']);
                        } else {
                            return $this->respondUnauthorised("Unauthorize Access Detected!");
                        }

                        break;

                    case 'teacher':
                        $result = $this->franchise->selectWhere($where, $select);

                        if ($result == NULL || empty($result) || $result == '') {
                            $data = $this->setUserAuthCookie($result, 'teacher-dashboard', 'teacher_id', $result->user_type);

                            return redirect($data['url'])
                                ->withCookie($data['cookie'])
                                ->withCookie($data['cookie_userid'])
                                ->withCookie($data['cookie_loginusertype']);
                        } else {
                            return $this->respondUnauthorised("Unauthorize Access Detected!");
                        }
                        break;

                    case 'admin':
                        $result = $this->franchise->selectWhere($where, $select);

                        if ($result == NULL || empty($result) || $result == '') {
                            $data = $this->setUserAuthCookie($result, 'admin-dasboard', 'admin_id', $result->user_type);

                            return redirect($data['url'])
                                ->withCookie($data['cookie'])
                                ->withCookie($data['cookie_userid'])
                                ->withCookie($data['cookie_loginusertype']);
                        } else {
                            return $this->respondUnauthorised("Unauthorize Access Detected!");
                        }
                        break;

                    default:
                        return $this->respondUnauthorised("Unauthorize Access Detected!");
                        break;
                }

            } else {
                return $this->respondUnauthorised("Unauthorize Access Detected!");
            }
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());
        }
    }

    private function setUserAuthCookie($result = array(), $route_name = 'dashboard', $route_param_name = '', $user_type)
    {
        $cookie = Cookie::make('tokenize',  $result->token);
        $cookie_userid = Cookie::make('cookieloginid', base64_encode($result->id));
        $cookie_loginusertype = Cookie::make('cookieloginusertype', base64_encode($user_type));
        $url = route($route_name, [$route_param_name => base64_encode($result->id)]);
        // $url = route('franchise-admin-list', [ 'franchise_id' => base64_encode($result->id) ]);
        return array("url" => $url, "cookie" => $cookie, "cookie_userid" => $cookie_userid, 'cookie_loginusertype' => $cookie_loginusertype);
    }
}
