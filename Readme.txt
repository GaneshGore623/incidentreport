Important Instruction To Setup Project:

1) Take the clone of repository.
2) Find the incident_db.sql file in same "database" folder and import by creating the db.
3) Change the db cred in .env file.
4) Run Composer update or install.
5) Run the project by "php artisan serve" Command. if you want then you can mention port by like this -  php artisan serve --port 8081 or anything.
6) Please import the given postman collection.
7) Finally you can check the project by using those api.

PHP 7+
Laravel : 8
MySQL: 8

Note: Project API devloped using repository pattern and dependancy injection method.

Thanks for giving me opportunity to perform this assignment. Hopefully I met your conditions.   