-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: incident_db
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `incident_category_master`
--

DROP TABLE IF EXISTS `incident_category_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `incident_category_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incident_category_master`
--

LOCK TABLES `incident_category_master` WRITE;
/*!40000 ALTER TABLE `incident_category_master` DISABLE KEYS */;
INSERT INTO `incident_category_master` VALUES (1,'Category 1','2021-06-24 00:29:45',NULL),(2,'Category 2','2021-06-24 00:19:48',NULL),(3,'Category 3','2021-06-24 00:06:52',NULL),(4,'Category 4','2021-06-23 23:36:54',NULL),(5,'Category 5','2021-06-24 00:08:58',NULL),(6,'Category 6','2021-06-23 23:46:59',NULL),(7,'Category 9','2021-06-24 01:13:08',NULL),(8,'a','2021-06-24 01:13:08',NULL),(9,'Category 9','2021-06-24 00:58:09',NULL);
/*!40000 ALTER TABLE `incident_category_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incident_report`
--

DROP TABLE IF EXISTS `incident_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `incident_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `category` varchar(45) DEFAULT NULL,
  `people` json DEFAULT NULL,
  `comments` varchar(45) DEFAULT NULL,
  `incident_date` datetime DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incident_report`
--

LOCK TABLES `incident_report` WRITE;
/*!40000 ALTER TABLE `incident_report` DISABLE KEYS */;
INSERT INTO `incident_report` VALUES (2,'19.4257','72.8374','Test Incident','1','[{\"name\": \"Test1\", \"type\": \"Staff\"}, {\"name\": \"Test2\", \"type\": \"Witness\"}]','Test Comment','2021-06-24 12:00:56','2021-06-24 06:30:56','2021-06-24 06:30:56'),(3,'19.4256','72.8373','Test Incident1','1','[{\"name\": \"Test3\", \"type\": \"Staff\"}, {\"name\": \"Test4\", \"type\": \"Witness\"}]','Test Comment','2021-06-24 12:00:56','2021-06-24 06:30:56','2021-06-24 06:30:56'),(4,'19.4257','72.8374','Test Incident1','1','[{\"name\": null, \"type\": null}, {\"name\": null, \"type\": null}]','Test Comment','2021-06-24 12:00:56','2021-06-24 06:30:56','2021-06-24 06:30:56'),(5,'19.4257','72.8374','Test Incident1','1','[{\"name\": \"Test5\", \"type\": \"Staff\"}, {\"name\": \"Test6\", \"type\": \"Witness\"}]','Test Comment','2021-06-24 12:00:56','2021-06-24 06:30:56','2021-06-24 06:30:56'),(6,'19.425743434','72.8374434343','Test Incident1','1','[{\"name\": \"Test5\", \"type\": \"Staff\"}, {\"name\": \"Test6\", \"type\": \"Witness\"}]','Test Comment','2021-06-24 12:00:56','2021-06-24 06:30:56','2021-06-24 06:30:56'),(7,'3213231232132','3213232323213','Test Incident1','1','[{\"name\": \"Test5\", \"type\": \"Staff\"}, {\"name\": \"Test6\", \"type\": \"Witness\"}]','Test Comment','2021-06-24 12:00:56','2021-06-24 06:30:56','2021-06-24 06:30:56'),(8,'3213231232132dasdsa','3213232323213','Test Incident1','1','[{\"name\": \"Test5\", \"type\": \"Staff\"}, {\"name\": \"Test6\", \"type\": \"Witness\"}]','Test Comment','2021-06-24 12:00:56','2021-06-24 06:30:56','2021-06-24 06:30:56'),(9,'19.4280','72.8356','Test Incident1','1','[{\"name\": \"Test5\", \"type\": \"Staff\"}, {\"name\": \"Test6\", \"type\": \"Witness\"}]','Test Comment','2021-06-24 12:00:56','2021-06-24 06:30:56','2021-06-24 06:30:56'),(10,'19.4280','72.8356','Test Incident1','1','[{\"name\": \"Test5\", \"type\": \"Staff\"}, {\"name\": \"Test6\", \"type\": \"Witness\"}]','Test Comment','2021-06-24 12:00:56','2021-06-24 06:30:56','2021-06-24 06:30:56');
/*!40000 ALTER TABLE `incident_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_master`
--

DROP TABLE IF EXISTS `type_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(45) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_master`
--

LOCK TABLES `type_master` WRITE;
/*!40000 ALTER TABLE `type_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `type_master` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-24 15:36:38
